---
layout: default
title: Substrate Formation
parent: Social Coding
nav_order: 2
permalink: process/focus/social-coding/substrate-formation
---

# Substrate Formation
{: .fs-9 .no_toc }

Decentralized ecosystems require close coordination of people and processes to evolve them.
{: .fs-6 .fw-500 }

<br>

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .challenge}
> Here we analyse the major challenge for the Fediverse of its near complete [Lack of a Common Technology Substrate](https://discuss.coding.social/t/challenge-healthy-substrate-formation-for-the-fediverse/63) that is required for a healthy ecosystem.

## What is substrate formation?

The term "substrate" is an imaginary foundation that underlies a technology or ecosystem is derived from this quote:

> “Any decentralized [ecosystem] **requires a centralized substrate**, and the more decentralized the approach is the more important it is that you can **count on the underlying system**.”
> <span style="font-size:60%">[&mdash; Byrne Hobart. The Promise and Paradox of Decentralization](https://www.thediff.co/p/the-promise-and-paradox-of-decentralization)</span>

The substrate constitutes all the people that collaborate to evolve the technology base, and the procedures they follow to do so. It encompasses the entirety of the formal and informal organization structure that exists around a project, an ecosystem, or a technology.

Substrate formation then is all activity that ensures this collaboration runs smoothly and in a way to contribute to the project / ecosystem / technology success factors in terms of adoption, evolution and growth.

## The Fediverse substrate

An investigation on the state of the Fediverse's own substrate painted a bleak picture:

| Open standard, community, website | Status |
| :--- | :--- |
| [W3C Social Web Incubator Community Group](https://www.w3.org/community/SocialCG/) | Inactive |
| [W3C ActivityStreams Core 2.0 open standard](https://www.w3.org/TR/activitystreams-core/) | Unmaintained |
| [W3C ActivityStreams Vocabulary 2.0 open standard](https://www.w3.org/TR/activitystreams-vocabulary/) | Unmaintained |
| [W3C ActivityPub open standard](https://www.w3.org/TR/activitypub/) | Unmaintained |
| [ActivityPub main website](https://activitypub.rocks/) | Outdated, unmaintained |
| [SocialHub developer community](https://socialhub.activitypub.rocks) | Low activity, inefficient |

Besides these major locations, on the whole Fediverse development is very fragmented and many of these initiatives have stalled. 

<details><summary>See here for more detailed information related to the Fediverse substrate (click to expand) ↲</summary>
<p markdown="block">

### [W3C Social Web Incubator Community Group ↲](https://www.w3.org/community/SocialCG/)

- The community group started at 2017-05-01, with currently 3 chairs (Amy Guy, Christine Webber, nightpool) and has 117 members.
- Last meeting was held in 2021-05-21 (Last [meeting minutes](https://www.w3.org/wiki/SocialCG#Meeting_minutes_archive) are of 7 May)
- The #social IRC channel does not have had any significant activity since mid 2021.
- The [CG Github organization](https://github.com/swicg) has 3 repositories, most recent activity an [issue closed](https://github.com/swicg/general/issues/27#event-4678998417) on 2021-05-03.
  - Unclear who are owners, maintainers and what their present role is wrt ActivityPub / Fediverse.

→ **Observation: Inactive**

### [W3C ActivityStreams Core 2.0 open standard ↲](https://www.w3.org/TR/activitystreams-core/)

- Became an official [W3C Recommendation](https://www.w3.org/TR/2017/REC-activitystreams-core-20170523/) on 2017-05-23.
- References [ActivityStreams Test Validator](https://as2.rocks/) last [known date](https://web.archive.org/web/2018*/https://as2.rocks/) (via archive.org) it was up was 2018-11-03.
  - Last [added implementation report was Pubstrate](https://github.com/w3c/activitystreams/commit/f643e90435b8027255356f79b84da052f1f6a320) in 2017-02-28, 16 project reports in total.
- [W3 ActivityStreams Core Github repository](https://github.com/w3c/activitystreams) has 52 open issues.
  - The repository has 39 commits since Recommendation status, some significant ones (like [adding `as:alsoKnownAs`](https://github.com/w3c/activitystreams/commit/54a6723ed1c8b95a12997b4e73100308e267fc8f)).
  - There are 9 open PR's, relevant issues, fixes suggested, that are not followed-up on (i.e. issue [#511 `as:alsoKnownAs`](https://github.com/w3c/activitystreams/issues/511)).
  - It is unclear who the maintainers of the repository are.

→ **Observation: Inactive, stalled**

### [W3C ActivityStreams Vocabulary 2.0 open standard ↲](https://www.w3.org/TR/activitystreams-vocabulary/)

- Became an official [W3C Recommendation](https://www.w3.org/TR/2017/REC-activitystreams-vocabulary-20170523/) on 2017-05-23, last [errata](https://github.com/w3c/activitystreams/commit/b4fb6d23fa3f8e257085182fbf00a1aabe02a1da) at 2019-05-9, 4 errata in total.
- [W3C ActivityStreams Github repository](https://github.com/w3c/activitystreams) shared with AS2-CORE, see above.

→ **Observation: Inactive, stalled**

### [W3C ActivityPub open standard ↲](https://www.w3.org/TR/activitypub/)

- Became an official [W3C Recommendation](https://www.w3.org/TR/2018/REC-activitypub-20180123/) on 2018-01-23.
- Referenced [suggestions for Auth/Authz best-practices](https://www.w3.org/wiki/SocialCG/ActivityPub/Authentication_Authorization) last modified 2017-11-21.
- Referenced [ActivityPub Test Suite](https://test.activitypub.rocks) last [known date](https://web.archive.org/web/20180101000000*/https://test.activitypub.rocks/) (via archive.org) it was up was 2018-12-21.
  - Last [added implementation report was Go-Fed](https://gitlab.com/dustyweb/activitypub.rocks/-/commit/db9a262d0cbed0683cf27d5f3a221ec4b34b4d11) in 2018-08-23, 13 project reports in total (2 marked 'ghost' on [apps watchlist](https://codeberg.org/fediverse/delightful-fediverse-apps)).
- [W3C ActivityPub Github repository](https://github.com/w3c/activitypub) has 49 open issues and most recent maintainer activity an [issue closed](https://github.com/w3c/activitypub/issues/344#event-2809052698) on 2019-11-18.
  - Unclear who maintainers are, but likely include [nightpool](https://github.com/nightpool) and [ap-socialhub](https://github.com/ap-socialhub).
  - The repository saw 2 minor commits since ActivityPub became a recommendation.

→ **Observation: Inactive, stalled**

### [ActivityPub Rocks website ↲](https://activitypub.rocks/)

- Has excellent SEO, but bare minimum information and links which make ActivityPub seem an inactive ecosystem.
- Infrequent updates, maintenance issues. [Code](https://gitlab.com/dustyweb/activitypub.rocks) by and domain owned by Christine Webber, hosting by other party (?)

→ **Observation: Inactive, too little informative**

### [SocialHub community ↲](https://socialhub.activitypub.rocks)

- Started 2019-10-11, 615 members in total, 237 in trust level 1 or higher.
- Past year (2021-02-17 / 2022-02-17):
  - Daily engaged users: 6, New contributors: 96, Signups: 255, Topics: 277, Posts: 2.7k, DAY/MAU: 21%
  - 91 members posted more than 1x, 45 more than 5x, 9 more than 50x, 25 members created more than 1 topic.
- Impressions of the community (as active forum moderator for 2 years). SocialHub:
  - Is not really a community, just a discussion forum to check now and then.
  - Adds value to individual projects who find useful information, or inspiration.
  - Adds little to Fediverse substrate, as most discussion linger, no decisions are made.
  - Most important for substrate formation are [Fediverse Enhancement Proposals](https://socialhub.activitypub.rocks/c/standards/fep/54) (FEP's).
  - FEP process is not well-established / adopted, not mature, nor very meaningful yet.

→ **Observation: Not very active or healthy**

### Other substrate related references

- [Mastodon](https://docs.joinmastodon.org/) as dominant application platform exhibits [post facto interoperability](https://en.wikipedia.org/wiki/Interoperability#Post_facto_interoperability), which has big influence on fedi evolution.
  - Mastodon (via Nightpool and Claire) interacts somewhat on SocialHub, but Eugen Rochko (the ['BDFL'](https://en.wikipedia.org/wiki/Benevolent_dictator_for_life)) does not.
  - Mastodon is pilot for [EUNOMIA](https://eunomia.social/) to deal with misinformation (see [announcement](https://blog.joinmastodon.org/2021/10/eunomia-public-pilot-launch/)) which may lead to further fedi specs.
- [Zot / Zap](https://codeberg.org/zot): By [Mike McGirvin](https://macgirvin.com/channel/mike). Range of mature apps evolved from [Hubzilla](https://hubzilla.org//page/hubzilla/hubzilla-project) (see [Mike's post](https://macgirvin.com/display/b64.aHR0cHM6Ly9tYWNnaXJ2aW4uY29tL2l0ZW0vMDc1NGQ2Y2YtYjFkMC00YzhlLTgyYTMtNjdiZTVjNWU1ZjZl)) with advanced Fediverse capabilities.
  - Mike has outspoken views on how FOSS should and should not be advocated. Project [Streams](https://codeberg.org/streams/streams) will replace other Zotlabs apps.
  - Mike has strong opinions on direction of the Fediverse, and also urges any substrate activity be on federated tools. Created [How to use the Fediverse for its own development?](https://socialhub.activitypub.rocks/t/howto-facilitate-the-fediverse-for-its-own-development/2367) on SocialHub.
- [GNU Social](https://www.gnusocial.rocks/v3/), the first federated social platform, is compatible with ActivityPub and also implements OStatus. See [Code](https://notabug.org/diogo/gnu-social)
- [Friendica](https://friendi.ca/) is compatible with ActivityPub, but uses a set of extensions that evolve independently, see [Docs](https://github.com/friendica/friendica/wiki).
- [FediDB](https://fedidb.org): By Pixelfed developer [@dansup](https://mastodon.social/@dansup). Very promising toolset, though [not yet readily available](https://socialhub.activitypub.rocks/t/how-to-compare-compatibility-with-other-activitypub-instances-using-fedidb/2257/2?u=aschrijver), but can be self-hosted.
- [Unofficial ActivityPub Testsuite](https://test.activitypub.dev/): By [Cory Slep](https://mastodon.technology/@cj) of Go-Fed. Has maintenance issues, not really maintained, but best option available.
- [LitePub](https://litepub.social/): Lightweight profile of ActivityPub, focus on easing security among others. Stalled since 2019-06-29.
- [Forgefriends community](https://forum.forgefriends.org): Domain-specific: Code forge federation. Not yet really active in substrate formation.
  - Co-shared community forum for: [Forgefriends](https://forgefriends.org) (code project, @dachary) and [ForgeFlux](https://forgeflux.io) (@realaravinth).
  - [Forgefriends FSDL](https://forum.forgefriends.org/c/incubator/forgefriends-fsdl/32) incubator project, just started, may become a collector of open specifications.
- [ForgeFed](https://forgefed.org): Domain-specific: Code forge federation. Recent intent to continue after long period of inactivity.
  - ForgeFed may become part of / adopted by Forgefriends community, may be forked, may be integrated with Forgefriends FSDL.
- [Podcastindex](https://github.com/Podcastindex-org/activitypub-spec-work) / [Minipub](https://minipub.dev/info/activitypub-for-podcast-apps/): Domain-specific: Podcasting. Community (informally?) led by [Adam Curry](https://en.wikipedia.org/wiki/Adam_Curry) exploring ActivityPub.
- [W3C OStatus Community Group](https://www.w3.org/community/ostatus/), closed on 2019-08-13, published [OStatus 1.0 Draft 2](http://ostatus.github.io/spec/OStatus%201.0%20Draft%202.html) ([PDF](https://www.w3.org/community/ostatus/wiki/images/9/93/OStatus_1.0_Draft_2.pdf)), [OStatus Github organization](https://github.com/ostatus).

### Fediverse in numbers

- Indicative number of nodes and fedizens (user accounts) depending on how they are collective. See also [Fediverse Party](https://fediverse.party/en/fediverse/).
  - [Fediverse Observer](https://fediverse.observer/stats) for ActivityPub: 4,709,338 accounts, steady rising stats in nodes, posts, accounts
  - [The Federation Info](https://the-federation.info/) for ActivityPub: 7963 nodes, 4,197,382 accounts
  - [Fediverse.to](https://www.fediverse.to/) for ActivityPub: 5102 nodes, 3,400,293 accounts, 119,055 active last week
- In April 2022 a big influx of new fedizens, migrating from Twitter due Elon Musk's intended buyout.
  - Very positive reception by newcomers. An increased uptick in developers interested to build federated apps.

  → **Observation: Good 'user-base', steady growth, few active developers**

</p>
</details>

