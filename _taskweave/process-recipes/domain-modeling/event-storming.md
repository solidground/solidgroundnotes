---
layout: default
title: Event Storming
parent: Domain Modeling
nav_order: 1
permalink: process/domain-modeling/event-storming
---

# Event Storming
{: .fs-9 .no_toc }

Collaborative workshop to quickly explore new domains and discover needs and use cases.
{: .fs-6 .fw-500 }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Summary

| | Event Storming allows to rapidly model processes during a workshop. The method is very useful to find requirements and get a shared understanding of important needs. |
| :--- | :--- |
| **When to use** | - At the start of your new social experience design.<br>- When adding new sub-domains to your social experience. |
| **Participants** | Representatives from both Client and Creator stakeholder groups. |
| **Requirements** | - Physical: Meeting room, whiteboards, pens and sticky notes.<br>- Remote: [Miro](https://miro.com) accounts and Miro Board configuration. |
| **Duration** | Initial session 4-6 hours. Multiple refinements afterwards. Keep up-to-date. |
| **Expected outcome** | Sticky note diagrams representing:<br>1. Domains.<br>2. Use cases.<br>3. Process flows. |

[➥ I need more info](/process/domain-modeling/#event-storming){: .btn .fs-5 .mb-4 .mb-md-0 }

## Step 1: Planning

Before you organize an Event Storming session give attention to this checklist:

- [ ] What are the objectives of the session?
- [ ] Is the scope of the domain sufficiently clear?
- [ ] Who must participate for the event to be most productive?

## Step 2: Organize the first session

### Organize a physical meeting

### Organize a remote event

If participants cannot physically meet in one room, the Event Storming can be done using collaborative online tools. Follow the preparation steps (Click for detailed instructions).

#### Preparation. Things you need

<details><summary><b>Step 2.A</b>:&nbsp; Prepare the Event Storming Board to use.</summary>
<p markdown="block">



</p>
</details>

<details><summary><b>Step 2.B</b>:&nbsp; Send invitations and instructions to participants.</summary>
<p markdown="block">



</p>
</details>

<details><summary><b>Step 2.C</b>:&nbsp; Ensure participants all have Miro access.</summary>
<p markdown="block">



</p>
</details>

{: .aside}
> A physical session is much more effective than event storming online. Alberto Brandolini in [Remote Event Storming](https://blog.avanscoperta.it/2020/03/26/remote-eventstorming/) explains the differences and how to get the most from online sessions.

## Step 3: During the session

### Introduction. Explain objectives and rules

### Get started. Elaborate diagrams in 3 iterations

#### 1st round: Big picture. Collect domain events

#### 2nd round: Big picture. Refine domain events

#### 3rd round: Process modeling. Track causes

### Taking stock: Prepare for social experience design

At this point a clear picture has emerged of the domain. The next activity is to relate the findings to the social experience that we want to design. The board must be refined so it is suitable for Creators to commence with software modeling.

#### Checklist for refining the board

- [ ] Which users trigger which commands?
- [ ] Which commands affect which aggregates or external systems and trigger which changes?
- [ ] Which aggregates or external systems trigger which events during command processing?
- [ ] Which events trigger which policies?
- [ ] Which events create which read models for which use cases?
- [ ] Which policies call up which new commands?

## Background information and resources

- This is a test

### We like to thank

The following people and information helped inspire and create this recipe:

- [Judith Birmoser](https://www.linkedin.com/in/judith-birmoser-4a634b83/) and her [Event Storming Board Template](https://miro.com/miroverse/event-storming/) in the Miroverse.



