<div align="center">

[![solidground logo](assets/images/solidground-logo-optimized.svg)](https://docs.solidground.work)

### [Project website](https://docs.solidground.work) | [Matrix space](https://matrix.to/#/#solidground-matters:matrix.org) | [Discussion forum](https://discuss.coding.social/c/solidground/13)

</div>

<br>

This is Solidground Notes. Documentation website for [Solidground](https://solidground.work) project.


## Contents

- [Contributing](#contributing)
  - [Site development](#site-development)
  - [Publishing](#publishing)
- [Credits](#credits)
- [Code of Conduct](#code-of-conduct)
- [License](#license)


## Contributing

Bug reports and pull requests are welcome. This project is intended to be a safe, welcoming space for collaboration. Please read our [Code of Conduct](#code-of-conduct).

We practice [Social Coding Principles](https://coding.social/principles). By participating in our
projects you consent to our [Community Participation Guidelines](CODE_OF_CONDUCT.md).).

Create an issue in our [Issue Tracker](https://codeberg.org/solidground/solidgroundnotes/issues) before you start to work on major changes, and check first if a similar issue doesn't already exist. If you want to add something entirely new, then it might be best to start a [forum discussion](https://discuss.coding.social) beforehand. Be sure to post in the Solidground Matters category or one of its subcategories.

### Site development

Install the prerequisites for building [Jekyll](https://jekyllrb.com/) websites, fork the repository to your Codeberg account and then clone that repository to your local computer.

To set up your environment for site development, first run `bundle install`.

To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server to preview your changes. Add pages, documents, data, etc. like normal to test your modified contents. As you make modifications to the site and its content, the site will regenerate and you should see the changes in the browser after a refresh..

When done editing and everything looks good, you can commit the changes to your Codeberg fork and create a Pull Request. Make sure that both your commit message and the PR description are succinct, clear and summarize the changes you've made.

If your PR relates to an Issue in the tracker, then mention the issue ID in the PR title between brackets e.g. "Improved accessibility (#11)".

### Publishing

The site maintainers will review your PR and publish to the website when all suggested changes are accepted.

## Credits

The Solidground Notes website is based on the [Just-the-Docs](https://pmarsceill.github.io/just-the-docs/) Jekyll theme. Many thanks to [Patrick Marsceill](http://patrickmarsceill.com/) and all theme [contributors](https://pmarsceill.github.io/just-the-docs/#thank-you-to-the-contributors-of-just-the-docs), and to [Codeberg](https://codeberg.org) for hosting great FOSS and this website.

### Contributors

See the list of [Contributors](CONTRIBUTORS.md) to the Solidground Notes website, and don't forget to add yourself to this list if you are one of them.

### Code of conduct

We practice [Social Coding Principles](https://coding.social/principles). By participating in our
projects you consent to our [Community Participation Guidelines](CODE_OF_CONDUCT.md).

## License

Copyright (c) 2022 [Solidground](https://solidground.work) and contributors.

Solidground Docs are licensed [AGPL-3.0](LICENSE) for project code, and [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/) for documentation.
