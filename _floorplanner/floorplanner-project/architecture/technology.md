---
layout: default
title: Technology choices
parent: Architecture
nav_order: 2
permalink: floorplanner/project/architecture/technology
---

# Floorplanner Technology Choices
{: .no_toc }

<details markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

## Introduction

{: .todo}
> Turn explanation into a process recipe.

{: .hint}
> Detailed information about [Using Technology Radars](#) can be found in our process recipe.

This page keeps track of major decisions that relate to Groundwork technology stack. Here we explore libraries and frameworks that are candidates to adopt in support of various architecture concepts and requirements. Decisions are ordered in reverse chronological order (Newest-first) and have states. They represent a [Technology Radar](https://www.thoughtworks.com/radar/faq-and-more). It has one additional 'Open' state, meaning that an investigation, called a Spike, is planned.:

<div markdown="1">
Adopt
{: .label .label-green }

Trial
{: .label .label-blue }

Assess
{: .label .label-yellow }

Hold
{: .label .label-red }

Open
{: .label .label-grey }
</div>

## Decision Log

### Adopt C4 for social experience modeling?

Open
{: .label .label-grey }

[C4 Models](https://c4model.com): "The C4 model was created as a way to help software development teams describe and communicate software architecture, both during up-front design sessions and when retrospectively documenting an existing codebase. It's a way to create maps of your code, at various levels of detail, in the same way you would use something like Google Maps to zoom in and out of an area you are interested in."

{: .issue}
> Adopt C4 Models for our project as well as SED solutions? ([housekeeping #10](https://codeberg.org/solidground/housekeeping/issues/10))

### Adopt AsyncAPI for Event Storming / CQRS representation?

Assess
{: .label .label-yellow }

[AsyncAPI](https://asyncapi.com) wants to be for Event Driven Architecture what OpenAPI is for REST API's. It is open effort to create an industry standard. How feasible is support of AsyncAPI to define the results from Event Storming sessions and refinement into CQRS/ES models.

Investigation notes can be found in the [AsyncAPI vs Event Storming](https://notes.smallcircles.work/xBsAYll5R5WT2zACog5fIA#) pad.

Decision was to revisit later. It looks promising but a good mapping is not yet possible, and the ecosystem not yet there.

{: .issue}
> AsyncAPI adoption? ([housekeeping #4](https://codeberg.org/solidground/housekeeping/issues/4))