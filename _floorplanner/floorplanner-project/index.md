---
layout: default
title: Floorplanner project
has_children: true
nav_order: 2
permalink: floorplanner/project
---

# Floorplanner
{: .fs-9 .no_toc }

Social experience design for the Floorplanner itself.
{: .fs-6 .fw-500 }

<details open markdown="block">
  <summary>
    Table of contents
  </summary>
  {: .text-delta }

- TOC
{:toc}
</details>

---

{: .info}
> This section is about Design and Development of the Floorplanner product. Refer to referenced [Housekeeping issues](https://codeberg.org/solidground/housekeeping/issues) related to Product, Process, Research and Architecture project tracks. **Everything in this documentation subject to change**.

## Domain overview

Taskweave and Floorplanner are tightly integrated. From the perspective of Creator and Client stakeholders they are both accessed from the Floorplanner UI. Yet they cover different top-level domains:

<table>
  <thead>
    <tr>
      <th width="40%">Product</th>
      <th width="60%">Top-level Domains</th>
    </tr>
  </thead>
  <tbody style="vertical-align: top;">
    <tr>
      <td><b>Taskweave</b> <p>Pattern library of best-practices supporting the FSDL.</p></td>
      <td><b>Free Software Development</b> (business domain)<br><br>
        System context (application domain):
        <ul>
          <li><b>Pattern Library</b>: Documentation framework for the presentation of Process Recipes.</li>
          <li><b>Playbook Designer</b>: Editing environment for the creation of process recipes and playbooks.</li>
          <li><b>Design System</b>: Storybook of user interface components for the presentation of the Pattern Library.</li>
        </ul>
      </td>
    </tr>
    <tr>
      <td><b>Floorplanner</b> <p>Design toolkit for blueprinting of social experiences.</p></td>
      <td><b>Social Experience Design</b> (business domain)<br><br>
        System context (application domain):
        <ul>
          <li><b>Blueprint Designer</b>: Management environment to work on the solution Blueprint.</li>
          <li><b>Collaborative Diagrammer</b>: Visual canvas to collectively create blueprint diagrams.</li>
          <li><b>Flow Processor</b>: Workflow engine to execute processes attached to recipes.</li>
          <li><b>Project Scaffolding</b>: Bootstrap project structure, generate code and configuration files.</li>
        </ul>
      </td>
    </tr>
  </tbody>
</table>

<br/>

### Domain: Free Software Development

The ultimate goal is for Floorplanner to support best-practices that relate to the entire Free Software Development Lifecycle (FSDL). The FSDL is contained in Taskweave, which make its top-level domain to be _Free Software Development_. It is important to understand that offering this support - in the form of numerous Process Recipes - is not part of the Solidground project directly.

Instead Solidground must offer support for the crowdsourced _creation of process recipes and playbooks_ in such way that they become available for Social Experience Design (SX). For the most part Creators will be responsible for filling the Pattern Library with process recipes and improving their quality and usability. This occurs as part of their own development process. That information is then input for "Kaizen continual improvement" of content contained in Taskweave.

### Domain: Social Experience Design

The top-level domain implemented by Floorplanner is Social Experience Design (SX). It is the primary service that is offered to Creators and Clients. Using Floorplanner the creators gradually evolve the Blueprint of a social experience based on a Playbook of Process Recipes they invoke. The playbook acts as a template that fits the desired development process for the FOSS project that creates the social experience, matching preferences of the development team. Playbooks can be part of the shared Pattern Library or custom configurations for a particular social experience project.

As Process Recipes from the playbook are invoked, the recipe guides creators along according to the best-practice. This may involve data entry of Blueprint state information, diagramming perspectives of the blueprint design, and following processes attach to the process recipe using the Flow Processor. All along the way the blueprint metadata is updated and keeps track of the state of the development process. Depending on the process flow of a process recipe, other process recipes may be invoked.

## Development process

### Dogfooding

An exciting aspect of Floorplanner is that it represents a social experience in itself! In other words is a social experience for the development of social experiences.

What should always be a key consideration in the development of Floorplanner, is that - since Floorplanner is a social experience itself - each process recipe is a building block that might improve Floorplanner's operation. And each feature added to Floorplanner indicates a potential process recipe to be identified, created and added to the pattern library. In other words process recipe development is integrated into the Floorplanner development process. But this is a rather intricate idea to wrap our heads around, and will be addressed better in future iterations.

{: .issue}
> Considering Floorplanner as a social experience itself is covered in [housekeeping #16](https://codeberg.org/solidground/housekeeping/issues/16).

### Minimum Lovable Process

Scope of Floorplanner is vast. For Floorplanner's inception we focus on small aspects of the FSDL. The baseline implementation is called the Minimum Lovable Process (MLP).

#### Main objectives

| **Deliver value** | The Floorplanner MLP has just enough functionality to make it usable in actual social experience design. |
| **Demonstrate concepts** | By using the Floorplanner MLP people gain a clear understanding of the project and its power and potential. |
| **Facilitates Kaizen** | Floorplanner MLP helps in discovery of Solidground's own project workflows to automate for dogfooding. |
| **Onboarding** | Use Floorplanner MLP to excite people to become active participants in Solidground's community and ecosystem. |

The scope of the MLP itself is not small enough to deliver in one release. We want onboarding to start right away and anyone interested to be able to follow along with our progress. Hence the MLP is delivered in stages.

#### MLP "Rough Sketch"

{: .note}
> The "rough sketch" is created as a prototype only for demo purposes. The codebase does not reflect the intended architecture.

The first stage of the Minimum Lovable process demonstrates most basic of concepts. The starting point of Domain Driven Design (DDD) is highlighted in a demo. The demo supports a workflow involving a number of Process Recipes:

1. Organize an [Event Storming](/process/recipes#event-storming) session to refine event-driven architecture of a social experience.
2. Parse resulting sticky notes diagrams and present Process Flows and Use Cases in Floorplanner.
3. Define Gherkin-based [Behaviour Tests](/process/recipes#behaviour-tests) and continue to [Scaffolding](/process/recipes#scaffolding) the project bootstrap.
4. Download the generated Elixir source code as a tarball or zipped package.

#### MLP "First Experience"

The "first experience" stage of the MLP focuses on the desired architecture, whereby the Floorplanner is an actual social experience. This lays the foundation for starting the process of Kaizen and dogfooding. The [Design](/floorplanner/project/design) section of this document starts with domain modeling of the Floorplanner i.e. elaborating the Social Experience Design top-level domain.